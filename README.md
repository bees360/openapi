# Bees360 API (Beta)

## Domain name

http://api.bees360.io

## Response Body and Status Code

Successful response example:

```js
200 OK
{
    "project": {
        "id": 10001,
    }
}
```

Error response example:

```js
400 BAD_REQUEST
{
    "error": {
        "message": "invailid project ID."
    }
}
```

## Authentication

To authenticate with Bees360 API, first request a `client_id` and a `client_secret` from Bees360 development team.

### Acquire Token

`POST /v1/oauth/token`

With the `client_id` and `client_secret`, a client can send a http `POST` request to acquire `access_token` and `refresh_token`. For example,

```shell
curl -X POST -vu myClientId:myClientSecret http://api.bees360.io/v1/oauth/token \
     -H "Accept: application/json" \
     -d "password=mypassword&username=myusername&grant_type=password&scope=mycompanyname"
```

A successful authorization results in the following JSON response:

```js
{
  "access_token": "ff16372e-38a7-4e29-88c2-1fb92897f558",
  "token_type": "bearer",
  "refresh_token": "f554d386-0b0a-461b-bdb2-292831cecd57",
  "expires_in": 43199,
  "scope": "mycompanyname"
}
```

### Refresh Token

`POST /v1/oauth/token`

```shell
curl -X POST -vu myClientId:myClientSecret http://api.bees360.io/v1/oauth/token \
     -H "Accept: application/json" \
     -d "grant_type=refresh_token&refresh_token=f554d386-0b0a-461b-bdb2-292831cecd57"
```

A successful authorization results in the following JSON response:

```js
{
  "access_token": "f276df18-cfc2-454e-a6f2-3ec63578c73c",
  "token_type": "bearer",
  "refresh_token": "fb22be4a-2b85-48b3-bbc3-54b91ada9f71",
  "expires_in": 43199,
  "scope": "mycompanyname"
}
```

### Use Token

An example:

```shell
curl http://api.bees360.io/v1/projects/1000012 \
    -H "Authorization: Bearer f276df18-cfc2-454e-a6f2-3ec63578c73c"
```

## Project API

### Create new project

`POST /v1/projects`

Request body:

```js
{
    "country": "USA",       // required
    "state": "NJ",          // required
    "city": "New York",     // required
    "zipcode": "07302",     // required
    "streetAddress": "125 Hudson St, APT 708", // required
    "houseType": "residential_single_family",
    "dueDate": "01/31/2020",
    "policyNumber": "1234567",
    "inspectionNumber": "12345678",
    "insuredName": "Jane Doe",
    "insuredPhone": "2232131877",
    "agentName": "John Smith",
    "agentPhone": "1234567891",
    "serviceType": "Quick Inspect Claim"
    ]
}
```

Example reponse:
```js
{
    "projects": [
        {
            "id": 10001,
            "country": "USA",
            "state": "NJ",
            "city": "New York",
            "zipcode": "07302",
            "streetAddress": "125 Hudson St, APT 708",
            "houseType": "residential_single_family",
            "dueDate": "01/31/2020",
            "policyNumber": "1234567",
            "inspectionNumber": "12345678",
            "insuredName": "Jane Doe",
            "insuredPhone": "2232131877",
            "agentName": "John Smith",
            "agentPhone": "1234567891",
            "serviceType": "Quick Inspect Claim"
        }
    ]
}
```

Valid values for house type:

```js
[
    "residential_single_family",
    "residential_condo",
    "residential_townhouse",
    "residential_multi_family",
    "residential_apartments",
    "commercial",
]
```
Valid values for service Name:

```js
[
    "Quick Inspect Claim",
    "Full Adjustment Claim",
    "Roof-only Underwriting",
    "Full-scope Underwriting",
]
```
### Get project latest status

`GET /v1/projects/{projectId}/latestStatus`

Example response:

```js
{
    "project": [
        {
            "id": 10001, // project id
            "latestStatus": "returned_to_client", // this is the final status for a project.
        }
    ]
}
```

Possible values for `latestStatus`:

```js
[
    "project_created",
    "customer_contacted",
    "assigned_to_pilot",
    "site_inspected",
    "returned_to_client",
]
```

### Get project images

`GET /v1/projects/{projectId}/images/archive`

Will return a redirect response, which redirects to a link to download the images archive in `.zip` format.

Example response:
```
302 FOUND
Content-Type: application/zip
Location: https://api.bees360.io/v1/projects/12345/files/original_images_xxxxxx.zip
...
```

### Get project report file

`GET /v1/reports/{reportId}/file`

Will return a redirect response, which redirects to a link to download the report file in `.pdf` format.

```
302 FOUND
Content-Type: application/pdf
Location: https://api.bees360.io/v1/reports/12345/file/roof_only_underwriting_report_xxxxxx.pdf
...
```

## Report List

| Report Name | Sample |
| --- | --- |
| Premium Damage Assessment Report | [Sample Report](https://test.bees360.com/index/report?type=1) |
| Premium Measurement Report | [Sample Report](https://test.bees360.com/index/report?type=2) |
| Real-time Damage Assessment Report | [Sample Report](https://test.bees360.com/index/report?type=8) |
| Roof-only Underwriting Report | [Sample Report](https://test.bees360.com/index/report?type=17) |
| Full-scope Underwriting Report | [Sample Report](https://test.bees360.com/index/report?type=18) |

